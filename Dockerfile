
FROM jupyterhub/jupyterhub as jupyterhub

ARG notebook_dir
ENV admin_name admin

RUN adduser --system --shell /bin/bash ${admin_name} && echo "${admin_name}:${admin_name}" | chpasswd ; mkdir -p ${notebook_dir}
RUN pip3 install jupyter_server 

CMD /usr/local/bin/jupyterhub --Spawner.notebook_dir=${notebook_dir}

